Documentation of source file
============================

.. currentmodule:: crer_scan

This page documents the source code of the crer_scan.py file. It is primarly intended for developers.

Summary
-------

.. autosummary::
   Site
   Siteft
   SiteSetOnSameSeq

Classes
-------
.. autoclass:: Site
   :members:

.. autoclass:: Siteft
   :members:

.. autoclass:: SiteSetOnSameSeq
   :members:
