Introduction to crer-scan
=========================

Description
-----------


**crer_scan** takes as input a set of "sites" (*genomic coordinates*), and reports *cis-regulatory enriched regions (CRER)*, i.e. genomic intervals containing a higher number of sites than expected by chance.

This tool can be used to predict cis-regulatory modules (CRM) from collections of transcription factor binding sites obatined by various methods.

    1. Predictions obtained by scanning sequences with position-specific scoring matrices (for example the output of matrix-scan
    2. Peaks from ChIP-seq experiments.
    3. Annotated binding sites imported from a transcription factor database.
    4. Any other data source that produces a set of genomic features.

**crer_scan** is part of the **RSAT (Regulatory Sequence Analysis Tools)**. A web interface is available `here`_.

.. _here: http://rsat.eu/


Autors and References
---------------------

Conception, implementation and testing: 

* Marie Artufel
* Lucie Khamvongsa
* Jacques van Helden

`Published in`_ ::
  
  Nucl. Acids Res. (1 July 2015) 43 (W1): W50-W56. 
  
  doi: 10.1093/nar/gkv362

.. _Published in: http://nar.oxfordjournals.org/content/43/W1/W50
