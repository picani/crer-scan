.. crer-scan documentation master file, created by
   sphinx-quickstart on Wed Jul 22 19:36:35 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to crer-scan's documentation!
=====================================

Contents:

.. toctree::
   :maxdepth: 2

   intro
   manual
   src_doc

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

