crer-scan
=========

**This program is a part of the Regulatory Sequence Analysis Tools ([RSAT](http://rsat.eu/)) suite.**

Written by Marie Artufel and Lucie Khamvongsa-Charbonnier.

Description 
------------

Scanning of predicted sites on sequence.
And Detection of putative cis-regulatory enriched regions (CRERs).

Dependances 
-----------

* Python
* [Scipy](http://www.scipy.org/index.html)

Install 
-------

Clone the repo :

	git clone https://picani@bitbucket.org/picani/crer-scan.git

Made the program executable :

	cd crer-scan
	chmod a+x src/crer_scan.py


Documentation 
-------------

The doc is made with [Sphinx](http://sphinx-doc.org/index.html) and is in the doc directory.

To build it :

	cd crer-scan/doc
	make html

Then open `crer-scan/doc/_build/html/index.html` with your browser.


Important Note 
--------------

This repo is NOT the official repo for crer-scan or RSAT. It is a proof-of-work for a new way to organize the dozens of programs available in RSAT. If you want to contact the RSAT team, go to the [main RSAT page](http://rsat.eu/).
